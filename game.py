import pygame

pygame.init()
window = pygame.display.set_mode((500, 500))

pygame.display.set_caption("Space Game")

x = 50
y = 50
width = 40
height = 50
speed = 5

run = True
while run:
    pygame.time.delay(100)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    
    pygame.draw.circle(window, (150, 0, 100),(x,y), 20)
    pygame.display.update()
pygame.quit()
